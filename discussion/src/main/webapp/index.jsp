<!-- Directives are defined by "%@" symbol to define the page attributes -->
<!-- Directives are also what we use to import packages, class in our JSP.  -->
<%@ 
	page language="java" 
	contentType="text/html; 
	charset=UTF-8"
    pageEncoding="UTF-8"
 	import="java.util.Date"   
 %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Jakarta Server Pages</title>
</head>
<body>
	<h1>Welcome to the Hotel Servlet</h1>
	<p>You can check out anytime you like, but you can never leave.</p>
	<!--JSP allows integration of HTML tags with Java Syntax  -->
	<!--JSP Scriptlets will allow us to add variables and execute java code  -->
	<% System.out.println("Hello form HSP"); %>
	<h2>The time now is: </h2>
	<% 
		Date currentDateTime = new java.util.Date();
		String name = "Tee Jae";
		String address = "Cainta, Rizal";
		int age = 20;
		
		
		
	%>
	<!--JSP expression tags will allow us to print the value of embedded/declared variables as string into an html element  -->
	<p><%= currentDateTime %></p>
	<h4>My Details</h4>
	<p>My name is <%= name %></p>
	<p>I live in <%= address %></p>
	<p>I am <%= age %></p>
	
	<!--JSP Declarations can also be used to create variables and methods  -->
	<%!
		private int initVar = 0;
		private int serviceVar = 0;
		private int destroyVar = 0;
		
		//Define the init and destroy methods of a jsp
		//In jsp, the init and destroy methods are named as jspInit and jspDestroy
		
		public void jspInit(){
			initVar++;
			System.out.println("jspInit(): init"+initVar);
		}
		
		public void jspDestroy(){
			destroyVar++;
			System.out.println("jspInit(): destroy:"+destroyVar);
			
		}
	%>
	
	<!--JSP scriplet declarations and definitions are actually added into internal jspService() method  -->	
	<!--Meanwhile, JSP declaration allows us to create variable and methods at a class level meaning it is added outside the _jspService() method -->
	
	<%
		System.out.println(age); // added into the _jspService() method
		serviceVar++;
		//Therefore everytime the _jspServicce method is used, serviceVar will increment
		
		String content1 = "content1: " + initVar;
		String content2 = "content2: " + serviceVar;
		String content3 = "content3: " + destroyVar;
		
	%>	
	
	<h1>JSP Expressions</h1>
	<p><%= content1 %></p>
	<p><%= content2 %></p>
	<p><%= content3 %></p>
	
	<br>
	
	<h1>Create an Account</h1>
	<form action="user" method="post">
		<label for="firstname">First Name:</label>
		<br>
		<input type="text" name="firstName" id="firstname">
		<br>
		<label for="lastname">Last Name:</label>
		<br>
		<input type="text" name="lastName" id="lastname">
		<br>
		<label for="email">Email:</label>
		<br>
		<input type="email" name="email" id="email">
		<br>
		<label for="contact">Contact:</label>
		<br>
		<input type="text" name="contact" id="contact">
		<br>
		<input type="submit">
	</form>
	
</body>
</html>



