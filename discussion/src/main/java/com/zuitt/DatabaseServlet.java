package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import jakarta.servlet.ServletContext;
//import jakarta.servlet.annotation.WebInitParam;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/database")
public class DatabaseServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6668827760365993356L;

	/**
	 * 
	 */
	
	ArrayList<String> users = new ArrayList<>();
	
	public void init() {
		
		System.out.println("*********************");
		System.out.println("DatabaseServlet initialized");
		System.out.println("Connected to Database");
		System.out.println("*********************");
		
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		ServletContext srvContext = getServletContext();
		String firstName = srvContext.getAttribute("firstName").toString();
		String lastName = srvContext.getAttribute("lastName").toString();
		String email = srvContext.getAttribute("email").toString();
		String contact = srvContext.getAttribute("contact").toString();
		
		users.add(firstName + ": " + lastName + ": " + email + " " + contact);
		
//		PrintWriter out = res.getWriter();
//		
//		out.println(
//		"<h1>User Details Saved in Database: </h1>" +
//		"<p>First Name: " + firstName + "</p>" +
//		"<p>Last Name: " + lastName + "</p>" +
//		"<p>Email: " + email + "</p>" +
//		"<p>Contact: " + contact + "</p>" 
//		
//		);
		
		res.sendRedirect("Database.jsp");
	}
	
	public void destroy() {
		
		System.out.println("*********************");
		System.out.println("DatabaseServlet finalized");
		System.out.println("Disconnected to Database");
		System.out.println("*********************");
		
	}

}






